import unittest
import json
import requests
import classes.connection as con


def getWeaponMasteryHelper():
    # connecting to known player
    with open('../util/keys.json') as kf:
        data = json.load(kf)

    if data['keys']['pubg']:
        pubg_key = data['keys']['pubg']
        headers = {
            'Authorization': 'Bearer ' + pubg_key,
            'Accept': 'application/vnd.api+json',
        }
        params = (
            ('filter[playerNames]', 'EBKChrist'),
        )

        response_player = requests.get('https://api.pubg.com/shards/' +
                                       'xbox-na' +
                                       '/players',
                                       headers=headers,
                                       params=params)
        pl = response_player.json()

        response_weapons = requests.get('https://api.pubg.com/shards/' +
                                        'xbox' +
                                        '/players/' +
                                        pl['data'][0]['id'][8:] +
                                        '/weapon_mastery',
                                        headers=headers)
        return response_weapons.json()


def getPlayerHelper():
    # connecting to known player
    with open('../util/keys.json') as kf:
        data = json.load(kf)

    if data['keys']['pubg']:
        pubg_key = data['keys']['pubg']
        headers = {
            'Authorization': 'Bearer ' + pubg_key,
            'Accept': 'application/vnd.api+json',
        }
        params = (
            ('filter[playerNames]', 'EBKChrist'),
        )

        response_player = requests.get('https://api.pubg.com/shards/' +
                                       'xbox-na' +
                                       '/players',
                                       headers=headers,
                                       params=params)
        return response_player.json()


class PubgConnectionTest(unittest.TestCase):
    def test_init(self):
        print("1 testing pubg connection init: ")
        conn = con.PubgConnection('testgt', 'testplatform')
        self.assertEqual(conn.gamertag, 'testgt')
        self.assertEqual(conn.platform, 'testplatform')
        self.assertEqual(conn.pubgKey, '')
        self.assertEqual(conn.keyFile, '../util/keys.json')
        self.assertEqual(conn.pubgEndpoint, 'https://api.pubg.com/shards/')
        self.assertEqual(conn.headers, {'Accept': 'application/vnd.api+json'})
        conn = con.PubgConnection('gamertag', 'xbox')
        self.assertEqual(conn.gamertag, 'gamertag')
        self.assertEqual(conn.platform, 'xbox')
        print("1 done")

    def loadKeysHelper(self):
        # known file location (in READ.ME)
        with open('../util/keys.json') as kf:
            data = json.load(kf)
        return data

    def test_loadkeys(self):
        print("2 testing pubg connection load keys: ")
        data = self.loadKeysHelper()

        conn = con.PubgConnection('gamertag', 'xbox')
        conn.loadKeys()
        self.assertEqual(conn.keyFile, '../util/keys.json')
        self.assertEqual(conn.pubgKey, data['keys']['pubg'])
        self.assertEqual(conn.headers, {
            'Authorization': 'Bearer ' + data['keys']['pubg'],
            'Accept': 'application/vnd.api+json',
        })
        print("2 done")

    def test_getPlayer(self):
        print("3 testing pubg connection get player: ")
        data = getPlayerHelper()
        conn = con.PubgConnection("EBKChrist", 'xbox-na')
        conn.loadKeys()
        self.assertEqual(conn.getPlayer(), data)
        self.assertEqual(conn.playerId, data['data'][0]['id'][8:])
        print("3 done")

    def test_getWeaponMastery(self):
        print("4 testing pubg connection get weapon mastery: ")
        data = getWeaponMasteryHelper()
        conn = con.PubgConnection("EBKChrist", 'xbox-na')
        conn.loadKeys()
        conn.getPlayer()
        self.assertEqual(conn.getWeaponMastery(), data)
        print("4 done")


if __name__ == '__main__':
    unittest.main()
