import json
import unittest

import requests

import classes.player as player


def getPlayerHelper():
    # connecting to known player
    with open('../util/keys.json') as kf:
        data = json.load(kf)

    if data['keys']['pubg']:
        pubg_key = data['keys']['pubg']
        headers = {
            'Authorization': 'Bearer ' + pubg_key,
            'Accept': 'application/vnd.api+json',
        }
        params = (
            ('filter[playerNames]', 'EBKChrist'),
        )

        response_player = requests.get('https://api.pubg.com/shards/' +
                                       'xbox-na' +
                                       '/players',
                                       headers=headers,
                                       params=params)
        return response_player.json()

def getWeaponMasteryHelper():
    # connecting to known player
    with open('../util/keys.json') as kf:
        data = json.load(kf)

    if data['keys']['pubg']:
        pubg_key = data['keys']['pubg']
        headers = {
            'Authorization': 'Bearer ' + pubg_key,
            'Accept': 'application/vnd.api+json',
        }
        params = (
            ('filter[playerNames]', 'EBKChrist'),
        )

        response_player = requests.get('https://api.pubg.com/shards/' +
                                       'xbox-na' +
                                       '/players',
                                       headers=headers,
                                       params=params)
        pl = response_player.json()

        response_weapons = requests.get('https://api.pubg.com/shards/' +
                                        'xbox' +
                                        '/players/' +
                                        pl['data'][0]['id'][8:] +
                                        '/weapon_mastery',
                                        headers=headers)
        return response_weapons.json()


class PubgPlayerTest(unittest.TestCase):

    def test_init(self):
        print("Testing Init")
        person = player.PubgPlayer("EBKChrist", 'xbox-na')
        self.assertEqual(person.name, "EBKChrist")
        self.assertEqual(person.data, getPlayerHelper())
        self.assertEqual(person.weapons, getWeaponMasteryHelper())



    def test_something(self):
        self.assertEqual(False, False)


if __name__ == '__main__':
    unittest.main()
