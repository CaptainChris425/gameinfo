import requests
import json

pubg_name = 'TGLTN'
xbox_na = 'pc-na'
pubg_endpoint = 'https://api.pubg.com/shards/'
pubg_key = ''

def getKeys(keys=["pubg"]):
    global pubg_key
    global pubg_name
    global pubg_endpoint
    global xbox_na
    with open('../util/keys.json') as kf:
        data = json.load(kf)

    if data['keys']['pubg']:
        pubg_key = data['keys']['pubg']
        headers = {
            'Authorization': 'Bearer ' + pubg_key,
            'Accept': 'application/vnd.api+json',
        }
        params = (
            ('filter[playerNames]', pubg_name),
        )

        response_player = requests.get(pubg_endpoint +
                                       xbox_na +
                                       '/players',
                                       headers=headers,
                                       params=params)
        pubg_player_data = response_player.json()
        print(pubg_player_data['data'][0]['id'][8:])
        response_weapons = requests.get(pubg_endpoint +
                                        'steam' +
                                        '/players/' +
                                        pubg_player_data['data'][0]['id'][8:] +
                                        '/weapon_mastery',
                                        headers=headers)
        pubg_weapon_data = response_weapons.content
        print(pubg_player_data)
        print('weapondata:')
        print(pubg_weapon_data)


def connect():
    getKeys()


