import json
import requests


class PubgConnection:
    keyFile = '../util/keys.json'
    pubgEndpoint = 'https://api.pubg.com/shards/'

    def __init__(self, gt, pf):
        self.gamertag = gt
        self.platform = pf
        self.pubgKey = ''
        self.headers = {
            'Accept': 'application/vnd.api+json',
        }
        self.playerId = ''

    def setup(self):
        self.loadKeys()

    def loadKeys(self):
        if self.pubgKey == '':
            with open(self.keyFile) as kf:
                data = json.load(kf)

            try:
                if data['keys']['pubg']:
                    self.pubgKey = data['keys']['pubg']
            except Exception as e:
                print("No pubg key found: " + str(e))

        self.headers = {
            'Authorization': 'Bearer ' + self.pubgKey,
            'Accept': 'application/vnd.api+json',
        }

    def getPlayer(self):
        params = (
            ('filter[playerNames]', self.gamertag),
        )

        response_player = requests.get(self.pubgEndpoint +
                                       self.platform +
                                       '/players',
                                       headers=self.headers,
                                       params=params)

        pubg_player_data = response_player.json()
        print(pubg_player_data)
        self.playerId = pubg_player_data['data'][0]['id'][8:]
        return pubg_player_data

    def getWeaponMastery(self):
        responseWeapons = requests.get(self.pubgEndpoint +
                                       'xbox' +
                                       '/players/' +
                                       self.playerId +
                                       '/weapon_mastery',
                                       headers=self.headers)
        return responseWeapons.json()
