from classes.connection import PubgConnection


class PubgPlayer:

    def __init__(self, gt, pf):
        self.name = gt
        self.connection = PubgConnection(gt, pf)
        self.connection.setup()
        self.data = self.connection.getPlayer()
        self.weapons = self.connection.getWeaponMastery()
        self.id = ''
        self.shardId = ''
        self.matches = ''

    def load(self):
        try:
            self.id = self.data['data'][0]['id'][8:]
        except KeyError as e:
            print("Unable to read key needed to load data")
            print(e)
        except IndexError as e:
            print("Unable to read value needed to load data")
            print(e)
